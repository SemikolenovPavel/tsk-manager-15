package ru.t1.semikolenov.tm.exception.field;

public final class DateEndIncorrectException extends AbstractFieldException {

    public DateEndIncorrectException() {
        super("Error! Data end is incorrect...");
    }

}
